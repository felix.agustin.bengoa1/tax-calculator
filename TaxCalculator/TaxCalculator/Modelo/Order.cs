﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxCalculator.Modelo
{
    public class Order
    {
        public Tax_Calculator tax_Calculator { get; set; }
        public List<Item> Items { get; set; }
        public Country? Country { get; set; }
        
        public State? State { get; set; }

        public Order(List<Item> items, Country country, State state) { 
            tax_Calculator = new Tax_Calculator();
            Items = items;
            Country = country;
            State = state;
        }

        public float GetTotalOrder()
        {
            float total = 0;

            foreach (var item in Items) {
                float subtotal = item.Precio * item.Cantidad;
                total += subtotal * tax_Calculator.GetTaxRate(Country, State, item.Product);
            }

            return total;
        }
    }   
}
