﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxCalculator.Modelo
{
    public class Item
    {
        public float Precio { get; set; }
        public int Cantidad {  get; set; }

        public Product Product { get; set; }

        public Item(float precio, int cantidad, Product product) { 
            Precio = precio;
            Cantidad = cantidad;
            Product = product;
        }
    }
}
