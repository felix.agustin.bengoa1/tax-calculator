﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxCalculator.Modelo
{
    public class Product
    {
        public float Precio {  get; set; }

        public string Nombre {  get; set; }
        public Product(float precio, string nombre) { 
            Precio = precio;
            Nombre = nombre;
        }
    }
}
