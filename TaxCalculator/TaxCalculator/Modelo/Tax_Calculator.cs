﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxCalculator.Modelo
{
    public class Tax_Calculator
    {
        public float GetTaxRate(Country country, State state, Product product) {
            float taxes = 0;

            if (country != null)
            {
                taxes = GetEuTax(country);
            }
            else if (state != null){
                taxes = GetUsTax(state);
            }
            else if (product != null)
            {
                taxes = GetChineaseTax(product);
            }

            return taxes;
        }

        private float GetEuTax(Country country)
        {
            return country.Tax;
        }

        private float GetUsTax(State state)
        {
            return state.Tax;
        }

        private float GetChineaseTax(Product product)
        {
            return product.Precio/2;
        }
    }
}
