﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxCalculator.Modelo
{
    public class State
    {
        public float Tax {  get; set; }
        public State(float tax) {
            Tax = tax;
        }
    }
}
