﻿namespace TaxCalculator
{
    partial class Vista
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BtnTesteo = new Button();
            lblNombre = new Label();
            LblPrecio = new Label();
            LblCantidad = new Label();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            SuspendLayout();
            // 
            // BtnTesteo
            // 
            BtnTesteo.Location = new Point(373, 41);
            BtnTesteo.Name = "BtnTesteo";
            BtnTesteo.Size = new Size(94, 29);
            BtnTesteo.TabIndex = 8;
            BtnTesteo.Text = "Testeo";
            BtnTesteo.UseVisualStyleBackColor = true;
            BtnTesteo.Click += BtnTesteo_Click;
            // 
            // lblNombre
            // 
            lblNombre.AutoSize = true;
            lblNombre.Location = new Point(373, 82);
            lblNombre.Name = "lblNombre";
            lblNombre.Size = new Size(12, 20);
            lblNombre.TabIndex = 9;
            lblNombre.Text = ".";
            // 
            // LblPrecio
            // 
            LblPrecio.AutoSize = true;
            LblPrecio.Location = new Point(373, 116);
            LblPrecio.Name = "LblPrecio";
            LblPrecio.Size = new Size(12, 20);
            LblPrecio.TabIndex = 10;
            LblPrecio.Text = ".";
            LblPrecio.Click += LblPrecio_Click;
            // 
            // LblCantidad
            // 
            LblCantidad.AutoSize = true;
            LblCantidad.Location = new Point(373, 145);
            LblCantidad.Name = "LblCantidad";
            LblCantidad.Size = new Size(12, 20);
            LblCantidad.TabIndex = 12;
            LblCantidad.Text = ".";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(215, 82);
            label1.Name = "label1";
            label1.Size = new Size(145, 20);
            label1.TabIndex = 13;
            label1.Text = "Total Order from EU:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(215, 116);
            label2.Name = "label2";
            label2.Size = new Size(145, 20);
            label2.TabIndex = 14;
            label2.Text = "Total Order from US:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(215, 145);
            label3.Name = "label3";
            label3.Size = new Size(142, 20);
            label3.TabIndex = 15;
            label3.Text = "Total order from Ch:";
            // 
            // Vista
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(LblCantidad);
            Controls.Add(LblPrecio);
            Controls.Add(lblNombre);
            Controls.Add(BtnTesteo);
            Name = "Vista";
            Text = "Vista";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Button BtnTesteo;
        private Label lblNombre;
        private Label LblPrecio;
        private Label LblCantidad;
        private Label label1;
        private Label label2;
        private Label label3;
    }
}
