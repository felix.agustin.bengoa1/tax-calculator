using TaxCalculator.Modelo;

namespace TaxCalculator
{
    public partial class Vista : Form
    {
        public Vista()
        {
            InitializeComponent();
        }

        private void LblPrecio_Click(object sender, EventArgs e) { }

        private void BtnTesteo_Click(object sender, EventArgs e)
        {
            /*
            List<Item> items = Datos.GenerateListItems();
            lblNombre.Text = items[0].Product.Nombre;
            LblPrecio.Text = string.Format(string.Format("{0:N2}", items[0].Precio));
            LblCantidad.Text = string.Format(string.Format("{0:N2}", items[0].Cantidad));
            LblTotal.Text = "patata";
            */
            List<Order> orders = Datos.GenerateNewOrders();
            lblNombre.Text = "$" + orders[0].GetTotalOrder().ToString();
            LblPrecio.Text = "$" + orders[1].GetTotalOrder().ToString();
            LblCantidad.Text = "$" + orders[2].GetTotalOrder().ToString();
        }
    }
}
