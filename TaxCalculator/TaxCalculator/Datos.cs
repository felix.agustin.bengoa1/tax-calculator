﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxCalculator.Modelo;

namespace TaxCalculator
{
    public static class Datos
    {

        
        public static List<Product> GenerateListProducts() {
            List<Product> list = new List<Product>();
            list.Add(new Product(100, "pan"));
            list.Add(new Product(200, "queso"));
            list.Add(new Product(6000, "coca"));
            return list;
        }

        public static List<Item> GenerateListItems()
        {
            List<Item> list = new List<Item>();
            List<Product> products = GenerateListProducts();
            list.Add(new Item(100, 1,products[0]));
            list.Add(new Item(200, 2, products[1]));
            return list;
        }

        public static List<Order> GenerateNewOrders()
        {
            List<Order> list = new List<Order>();
            List<Item> items = GenerateListItems();
            List<Country> countries = GenerateListCountry();
            List<State> states = GenerateListState();

            list.Add(new Order(items, countries[0], null));
            list.Add(new Order(items, null, states[0]));
            list.Add(new Order(items, null, null));

            return list;
        }

        public static List<Country> GenerateListCountry()
        {
            List<Country> list = new List<Country>();
            list.Add(new Country((float)0.9));
            return list;
        }
        public static List<State> GenerateListState()
        {
            List<State> list = new List<State>();
            list.Add(new State((float)0.1));
            return list;
        }
    }
}
